const templatesPath = 'plop-templates/';

module.exports = function (plop) {
  plop.setGenerator('React component', {
    "description": "generate a react component with test",
    "prompts": [{
      "type": "input",
      "name": "name",
      "message": "Specify component name please"
    },{
      "type": "input",
      "name": "destination",
      "message": "Specify destination path please",
      "default": "src/components"
    }],
    "actions": [{
      "type": "addMany",
      "destination": "{{destination}}/{{properCase name}}",
      "base": `${templatesPath}component/`,
      "templateFiles": `${templatesPath}component/*`
    }]
  });
};
